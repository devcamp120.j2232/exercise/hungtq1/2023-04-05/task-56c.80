package com.devcamp.musicapi.model;

import java.util.List;

public class Artist extends Comperser {
    private List<Album> albums;

    public Artist(String firstname, String lastname, String stagename, List<Album> albums) {
        super(firstname, lastname, stagename);
        this.albums = albums;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    @Override
    public String toString() {
        return "Artist [albums=" + albums + "]";
    }

}
