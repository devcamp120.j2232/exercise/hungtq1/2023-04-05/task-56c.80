package com.devcamp.musicapi.model;

public class Comperser extends Person {

    private String Stagename; 

    public Comperser(String firstname, String lastname, String stagename) {
        super(firstname, lastname);
        Stagename = stagename;
    }

    public String getStagename() {
        return Stagename;
    }

    public void setStagename(String stagename) {
        Stagename = stagename;
    }

    @Override
    public String toString() {
        return "Comperser [Stagename=" + Stagename + "]";
    }

}
